package com.supinfo.gameoflife;

/**
 * Created by PIERRE-LOUIS Antonio on 14/10/2015.<br/><br/>
 * Interface representing the action of a Cell in a Corway Life Game
 */
public interface Cell {
    /**
     * Return the corresponding cell for next generation.
     *
     * @param p_nbNeighbours Number of alive neighbours.
     * @return {@link Cell} for the next generation.
     */
    Cell newGeneration(int p_nbNeighbours);

    /**
     * Return a string symbol for the Cell.
     *
     * @return String symbol of the Cell.
     */
    String getAsString();

    /**
     * @return true if cell is alive, false otherwise.
     */
    boolean isAlive();

}
