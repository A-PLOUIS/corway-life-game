package com.supinfo.gameoflife;

/**
 * Created by PIERRE-LOUIS Antonio on 14/10/2015.<br/><br/>
 * Main Class of Application. Serve as launcher.
 */
public class Launcher {
    public static void main(String[] args) {

        World zaWarudo = new World(15, 5);
        System.out.println("Generation 1");
        System.out.print(zaWarudo.toString());
        zaWarudo.newGeneration();
        System.out.println("Generation 2");
        System.out.print(zaWarudo.toString());
        zaWarudo.newGeneration();
        System.out.println("Generation 3");
        System.out.print(zaWarudo.toString());
        zaWarudo.newGeneration();
        System.out.println("Generation 4");
        System.out.print(zaWarudo.toString());
        zaWarudo.newGeneration();
        System.out.println("Generation 5");
        System.out.print(zaWarudo.toString());
    }
}
