package com.supinfo.gameoflife;

/**
 * Created by PIERRE-LOUIS Antonio on 14/10/2015.
 */
public class DeadCell implements Cell {

    /**
     * According to rules : 3 neighbours are alive, we resurrect. Else we do nothing.
     *
     * @param p_nbNeighbours Number of alive neighbours.
     * @return {@link Cell} for the next generation.
     */
    @Override
    public Cell newGeneration(int p_nbNeighbours) {
        if (p_nbNeighbours == 3) {
            //Resurrection YAY!
            return new AliveCell(true);
        } else {
            //Still dead
            return this;
        }
    }

    @Override
    public String getAsString() {
        return "-";
    }

    @Override
    public boolean isAlive() {
        return false;
    }


}
