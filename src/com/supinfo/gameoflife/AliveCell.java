package com.supinfo.gameoflife;

/**
 * Created by PIERRE-LOUIS Antonio on 14/10/2015.<br/><br/>
 * Class representing an Alive Cell.<br/>
 * Note: an ALive Cell can be a "New born" one.
 */
public class AliveCell implements Cell {
    private boolean m_isNewBorn;

    public AliveCell(boolean p_isNewBorn) {
        this.m_isNewBorn = p_isNewBorn;
    }

    public AliveCell() {
        this(false);
    }

    /**
     * According to rules : if less than 2 or more than 3 neighbours are alive, we die. Else we live.
     *
     * @param p_nbNeighbours Number of alive neighbours.
     * @return {@link Cell} for the next generation.
     */
    @Override
    public Cell newGeneration(int p_nbNeighbours) {
        Cell nextCell;
        if (p_nbNeighbours < 2) {
            nextCell = new DeadCell();
        } else if (p_nbNeighbours <= 3) {
            nextCell = new AliveCell();
        } else {
            nextCell = new DeadCell();
        }
        return nextCell;
    }

    /**
     * @return "0" if a new born cell, "+" otherwise
     */
    @Override
    public String getAsString() {
        if (m_isNewBorn) {
            return "0";
        } else {
            return "+";
        }
    }

    @Override
    public boolean isAlive() {
        return true;
    }
}
