package com.supinfo.gameoflife;

import java.util.Random;

/**
 * Created by PIERRE-LOUIS Antonio on 14/10/2015.<br/><br/>
 * A World contains an Array of Cells and manage their evolution through the generations.
 */
public class World {
    private Cell[][] m_world;

    /**
     * Create a new world with a random content of Dead/New born cells
     *
     * @param nbColumn Number of column in word
     * @param nbRows   Number of row in world
     */
    public World(int nbColumn, int nbRows) {
        this.m_world = new Cell[nbRows][nbColumn];
        Random randomCellGenrator = new Random();
        for (int i = 0; i < nbRows; i++) {
            for (int j = 0; j < nbColumn; j++) {
                //Create random type of cell
                Cell newCell;
                switch (randomCellGenrator.nextInt(2)) {
                    case 0:
                        newCell = new DeadCell();
                        break;
                    case 1:
                        newCell = new AliveCell(true);
                        break;
                    default:
                        newCell = new DeadCell();
                }
                m_world[i][j] = newCell;
            }
        }
    }

    public World(Cell[][] p_world) {
        this.m_world = p_world;
    }

    /**
     * Create and return a String representing the world's grid
     *
     * @return The string representation of the world
     */
    @Override
    public String toString() {
        String worldContent = "";
        for (int i = 0; i < m_world.length; i++) {
            for (int j = 0; j < m_world[i].length; j++) {
                worldContent += m_world[i][j].getAsString() + " ";
            }
            worldContent += '\n';
        }

        return worldContent;
    }

    /**
     * Generate a new generation of cells from the current world.
     */
    public void newGeneration() {
        Cell[][] newWorld = new Cell[m_world.length][];

        for (int i = 0; i < m_world.length; i++) {
            //Create a new array for the new row
            newWorld[i] = new Cell[m_world[i].length];
            for (int j = 0; j < m_world[i].length; j++) {
                //Create new cell according to living neighbours.
                newWorld[i][j] = m_world[i][j].newGeneration(getAliveNeighbourForCell(i, j));
            }
        }
        this.m_world = newWorld;
    }

    /**
     * Get all the neigbours of a cell (UP, DOWN, LEFT, RIGHT, UP-LEFT, UP-RIGHT, DOWN-LEFT, DOWN-RIGHT),
     * check if they are alive and return the number of living cell around.
     *
     * @param p_column Column index
     * @param p_row    Row index
     * @return Number of neighbours alive
     */
    private int getAliveNeighbourForCell(int p_column, int p_row) {
        int aliveNeighboursCount = 0;
        if (p_row > 0) {
            //Left
            aliveNeighboursCount += m_world[p_column][p_row - 1].isAlive() ? 1 : 0;
        }
        if (p_row < m_world[p_column].length - 1) {
            //Right
            aliveNeighboursCount += m_world[p_column][p_row + 1].isAlive() ? 1 : 0;
        }
        if (p_column > 0) {
            //Up
            aliveNeighboursCount += m_world[p_column - 1][p_row].isAlive() ? 1 : 0;

            //Up-Left
            if (p_row > 0) {
                aliveNeighboursCount += m_world[p_column - 1][p_row - 1].isAlive() ? 1 : 0;
            }
            //Up-Right
            if (p_row < m_world[p_column].length - 1) {
                aliveNeighboursCount += m_world[p_column - 1][p_row + 1].isAlive() ? 1 : 0;
            }
        }
        if (p_column < m_world.length - 1) {
            //Down
            aliveNeighboursCount += m_world[p_column + 1][p_row].isAlive() ? 1 : 0;

            //Down-Left
            if (p_row > 0) {
                aliveNeighboursCount += m_world[p_column + 1][p_row - 1].isAlive() ? 1 : 0;
            }
            //Down-Right
            if (p_row < m_world[p_column].length - 1) {
                aliveNeighboursCount += m_world[p_column + 1][p_row + 1].isAlive() ? 1 : 0;
            }
        }

        return aliveNeighboursCount;
    }
}
